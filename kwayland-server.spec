%global  wayland_min_version 1.3

Name:           kwayland-server
Version:        5.24.7
Release:        2
Summary:        Wayland server components built on KDE Frameworks

License:        LGPLv2+ and MIT and BSD
URL:            https://invent.kde.org/plasma/%{name}

%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0:        http://download.kde.org/%{stable}/plasma/%{version}/%{name}-%{version}.tar.xz
BuildRequires:  libxml2
BuildRequires:  qt5-qtbase-devel

BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-kwayland-devel
BuildRequires:  wayland-devel
BuildRequires:  wayland-protocols-devel

BuildRequires:  plasma-wayland-protocols-devel >= 1.2
BuildRequires:  kf5-kwindowsystem-devel
BuildRequires:  kf5-kguiaddons-devel
BuildRequires:  qt5-qtwayland-devel

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1


%build
%{cmake_kf5}
%cmake_build

%install
%cmake_install


%ldconfig_scriptlets

%files
%license LICENSES/*
%{_kf5_datadir}/qlogging-categories5/kwaylandserver.categories
%{_kf5_libdir}/libKWaylandServer.so.*

%files devel
%{_kf5_libdir}/libKWaylandServer.so
%{_includedir}/KWaylandServer/
%{_includedir}/kwaylandserver_version.h
%{_libdir}/cmake/KWaylandServer/

%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.24.7-2
- adapt to the new CMake macros to fix build failure

* Mon Dec 12 2022 lijian <lijian2@kylinos.cn> - 5.24.7-1
- update to upstream version 5.24.7

* Tue Jul 5 2022 huayadong<huayadong@kylinos.cn> - 5.24.5-1
- update to upstream version 5.24.5

* Tue Feb 22 2022 pei-jiankang<peijiankang@kylinos.cn> - 5.24.0-2
- update to version 5.24.0-2

* Sat Feb 12 2022 pei-jiankang<peijiankang@kylinos.cn> - 5.24.0-1
- update to upstream version 5.24.0

